// grpclog provides interceptors for gRPC that log errors.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package grpclog

import (
	"context"
	"google.golang.org/grpc"
)

// Printer is the interface satisfied by the Printf method.
type Printer interface {
	Printf(format string, v ...interface{})
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// UnaryServerInterceptor returns the unary server interceptor logging to l.
func UnaryServerInterceptor(l Printer) grpc.UnaryServerInterceptor {
	if l == nil {
		panic("Illegal nil argument")
	}
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		resp, err := handler(ctx, req)
		if err != nil && l != nil {
			l.Printf("[%s] %s", info.FullMethod, err)
			return nil, err
		}
		return resp, err
	}
}

// StreamServerInterceptor returns the stream server interceptor logging to l.
func StreamServerInterceptor(l Printer) grpc.StreamServerInterceptor {
	if l == nil {
		panic("Illegal nil argument")
	}
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		err := handler(srv, stream)
		if err != nil && l != nil {
			l.Printf("[%s] %s", info.FullMethod, err)
		}
		return err
	}
}

// UnaryClientInterceptor returns the unary client interceptor logging to l.
func UnaryClientInterceptor(l Printer) grpc.UnaryClientInterceptor {
	if l == nil {
		panic("Illegal nil argument")
	}
	return func(ctx context.Context, method string, req interface{}, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		err := invoker(ctx, method, req, reply, cc, opts...)
		if err != nil && l != nil {
			l.Printf("[%s] %s", method, err)
		}
		return err
	}
}

// StreamClientInterceptor returns the stream client interceptor logging to l.
func StreamClientInterceptor(l Printer) grpc.StreamClientInterceptor {
	if l == nil {
		panic("Illegal nil argument")
	}
	return func(ctx context.Context, desc *grpc.StreamDesc, cc *grpc.ClientConn, method string, streamer grpc.Streamer, opts ...grpc.CallOption) (grpc.ClientStream, error) {
		stream, err := streamer(ctx, desc, cc, method, opts...)
		if err != nil && l != nil {
			l.Printf("[%s] %s", method, err)
		}
		return stream, err
	}
}
