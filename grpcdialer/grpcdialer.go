// grpcdialer provides client gRPC ContextDialer implementations.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package grpcdialer

import (
	"context"
	"google.golang.org/grpc"
	"net"
	"nhooyr.io/websocket"
	"strconv"
	"time"
)

// DialWithContextFunc defines a function that can be used to dial a connection.
type DialWithContextFunc func(ctx context.Context) (net.Conn, error)

// Printer is the interface satisfied by the Printf method.
type Printer interface {
	Printf(format string, v ...interface{})
}

// maxDialAttempts is the maximum number of attempts retryDialWithContext will make to dial a connection.
const maxDialAttempts = 10

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// defaultTCPDialWithContextFunc returns a DialWithContextFunc that calls
//	net.Dial("tcp", host + ":" + port)
// Logging messages will be sent to lg, if non-nil.
func defaultTCPDialWithContextFunc(host string, port int, lg Printer) DialWithContextFunc {
	return func(ctx context.Context) (net.Conn, error) {
		if lg != nil {
			lg.Printf("Dialling %s:%d...", host, port)
		}
		conn, err := (&net.Dialer{}).DialContext(ctx, "tcp", host+":"+strconv.Itoa(port))
		if err != nil && lg != nil {
			lg.Printf("Error establishing connection: %s", err)
		}
		return conn, err
	}
}

// retryDialWithContext attempts to dial a connection using the given dial function. If the dial fails, it will be retried with exponential back-off. The exponential back-off caps out at approximately 1 minute. This will try maxDialAttempts times before abandoning the attempt; you can use the context to cancel the attempt sooner.
func retryDialWithContext(dial DialWithContextFunc) DialWithContextFunc {
	return func(ctx context.Context) (net.Conn, error) {
		// Loop with exponential back-off
		wait := time.Second
		attempt := 1
		for {
			// Attempt to dial a connection
			conn, err := dial(ctx)
			if err == nil {
				return conn, nil
			} else if err == context.Canceled || err == context.DeadlineExceeded {
				return nil, err
			}
			// Should we abandon our attempts?
			if attempt == maxDialAttempts {
				return nil, err
			}
			// Wait
			select {
			case <-ctx.Done():
				return nil, ctx.Err()
			case <-time.After(wait):
				attempt++
			}
			// Double the wait time, if appropriate, and retry
			if wait < time.Minute {
				wait = 2 * wait
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// WithContextDialer returns a grpc.DialOption for the given DialWithContextFunc.
func WithContextDialer(dial DialWithContextFunc) grpc.DialOption {
	return grpc.WithContextDialer(
		func(ctx context.Context, _ string) (net.Conn, error) {
			return dial(ctx)
		},
	)
}

// WithRetryContextDialer returns a grpc.DialOption for the given DialWithContextFunc. The dial will be retried on error.
func WithRetryContextDialer(dial DialWithContextFunc) grpc.DialOption {
	return WithContextDialer(retryDialWithContext(dial))
}

// TCPDialer returns a grpc.DialOption for connecting to a TCP socket with given address and port number. Logging messages will be sent to lg, if non-nil. The dial will be retried on error.
func TCPDialer(address string, port int, lg Printer) grpc.DialOption {
	return WithRetryContextDialer(
		defaultTCPDialWithContextFunc(address, port, lg),
	)
}

// WebSocketDialer returns a grpc.DialOption for connecting to a web-socket with given URI. Logging messages will be sent to lg, if non-nil. The dial will be retried on error.
func WebSocketDialer(uri string, lg Printer) grpc.DialOption {
	return WithRetryContextDialer(
		func(ctx context.Context) (net.Conn, error) {
			if lg != nil {
				lg.Printf("Dialling %s...", uri)
			}
			c, _, err := websocket.Dial(ctx, uri, nil)
			if err != nil {
				if lg != nil {
					lg.Printf("Error establishing connection: %v", err)
				}
				return nil, err
			}
			return websocket.NetConn(
				context.Background(), c, websocket.MessageBinary,
			), nil
		},
	)
}
