// grpcs2 registers the S2 compression with gRPC.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package grpcs2

import (
	"bitbucket.org/pcastools/compress"
	_ "bitbucket.org/pcastools/compress/s2"
	"google.golang.org/grpc/encoding"
	"io"
)

// Name is the name under which the compression format is registered with gRPC.
const Name = "S2"

// compressor is a gRPC compressor using S2.
type compressor int

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the compressor with gRPC.
func init() {
	encoding.RegisterCompressor(compressor(0))
}

/////////////////////////////////////////////////////////////////////////
// compressor functions
/////////////////////////////////////////////////////////////////////////

// Name is the name of the compression codec and is used to set the content coding header.
func (compressor) Name() string {
	return Name
}

// Compress writes the data written to wc to w after compressing it.
func (compressor) Compress(w io.Writer) (io.WriteCloser, error) {
	return compress.S2.NewWriter(w)
}

// Decompress reads data from r, decompresses it, and provides the uncompressed data via the returned io.Reader.
func (compressor) Decompress(r io.Reader) (io.Reader, error) {
	return compress.S2.NewReader(r)
}
