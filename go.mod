module bitbucket.org/pcastools/grpcutil

go 1.13

require (
	bitbucket.org/pcastools/compress v1.0.5
	golang.org/x/net v0.9.0 // indirect
	google.golang.org/genproto v0.0.0-20230403163135-c38d8f061ccd // indirect
	google.golang.org/grpc v1.54.0
	nhooyr.io/websocket v1.8.7
)
